package io.muic.ooc.webapp.service;

import io.muic.ooc.webapp.repository.CountyRepository;
import io.muic.ooc.webapp.repository.PlaceRepository;
import io.muic.ooc.webapp.repository.StateRepository;
import io.muic.ooc.webapp.entity.County;
import io.muic.ooc.webapp.entity.State;
import io.muic.ooc.webapp.entity.Place;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PlaceService {

    @Autowired
    private StateRepository stateRepository;

    @Autowired
    private CountyRepository countyRepository;

    @Autowired
    private PlaceRepository placeRepository;

    public void importSinglePlace(String stateName, String stateAbbreviation,
            String countyName, String placeName, String postalCode, Double latitude, Double longtitude) {
        State state = addStateIfAbsent(stateName, stateAbbreviation);
        County county = addCountyIfAbsent(state, countyName);
        addPlaceIfAbsent(county, placeName, postalCode, latitude, longtitude);
    }

    @Transactional
    public void batchImportPlaces() {
        InputStream ins = null;
        try {
            ins = getClass().getResourceAsStream("/us_postal_codes_data.csv");
            Reader reader = new InputStreamReader(ins, "UTF-8");
            CSVParser parser = new CSVParser(reader, CSVFormat.EXCEL.withHeader());
            for (final CSVRecord record : parser) {
                String postCode = record.get("Postal Code");
                String placeName = record.get("Place Name");
                String stateName = record.get("State");
                String stateAbbreviation = record.get("State Abbreviation");
                String county = record.get("County");
                String latitudeStr = record.get("Latitude");
                String longtitudeStr = record.get("Longitude");
                Double latitude = null;
                Double longtitude = null;
                try {
                    latitude = NumberUtils.createDouble(latitudeStr);
                    longtitude = NumberUtils.createDouble(longtitudeStr);
                } catch (Exception e) {
                }
                importSinglePlace(stateName, stateAbbreviation, county, placeName, postCode, latitude, longtitude);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            IOUtils.closeQuietly(ins);
        }
    }

    public State addStateIfAbsent(String stateName, String stateAbbreviation) {
        State state = stateRepository.findByName(stateName);
        if (state == null) {
            state = new State();
            state.setAbbrevition(stateAbbreviation);
            state.setName(stateName);
            state = stateRepository.save(state);
        }
        return state;
    }

    public County addCountyIfAbsent(State state, String countyName) {
        County county = countyRepository.findByName(countyName);
        if (county == null) {
            county = new County();
            county.setName(countyName);
            county.setState(state);
            county = countyRepository.save(county);
        }
        return county;
    }

    public void addPlaceIfAbsent(County county, String placeName, String postalCode, Double latitude, Double longtitude) {
        List<Place> places = placeRepository.findByName(placeName);
        if (places == null || places.isEmpty()) {
            Place place = new Place();
            place.setCounty(county);
            place.setLatitude(latitude);
            place.setLongtitude(longtitude);
            place.setName(placeName);
            place.setPostalCode(postalCode);
            placeRepository.save(place);
        }
    }

    public List<Place> searchPlaces(String q) {
        List<Place> places = placeRepository.findByNameContaining(q);
        return places;
    }

}
