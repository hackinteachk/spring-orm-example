/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.muic.ooc.webapp.repository;

import io.muic.ooc.webapp.entity.County;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author gigadot
 */
@Repository
public interface CountyRepository extends CrudRepository<County, Long>{

    public County findByName(String countyName);
    
}
