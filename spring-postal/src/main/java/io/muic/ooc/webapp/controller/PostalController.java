package io.muic.ooc.webapp.controller;

import io.muic.ooc.webapp.service.PlaceService;
import io.muic.ooc.webapp.entity.Place;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PostalController {

    @Autowired
    private PlaceService placeService;

    @GetMapping(value = {"/"})
    public long index() {
        long startTime = System.currentTimeMillis();
        placeService.batchImportPlaces();
        return System.currentTimeMillis() - startTime;
    }

    @GetMapping(value = {"/search_places"})
    public Map searchPlaces(String q) {
        long startTime = System.currentTimeMillis();
        List<Place> places = placeService.searchPlaces(q);
        Map<String, Object> response = new HashMap<>();
        Set<String> counties = new HashSet<>();
        Set<String> states = new HashSet<>();
        for (Place place : places) {
            counties.add(place.getCounty().getName());
            states.add(place.getCounty().getState().getName());
        }
        
        response.put("states", MapUtils.putAll(new HashMap<>(), new Object[] {
            "count", states.size(),
            "results", states
        }));
        response.put("counties", MapUtils.putAll(new HashMap<>(), new Object[] {
            "count", counties.size(),
            "results", counties
        }));
        response.put("places", MapUtils.putAll(new HashMap<>(), new Object[] {
            "count", places.size(),
            "results", places
        }));
        response.put("executionTime", System.currentTimeMillis() - startTime);
        return response;
    }

}
