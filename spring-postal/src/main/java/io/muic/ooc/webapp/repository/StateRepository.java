package io.muic.ooc.webapp.repository;

import io.muic.ooc.webapp.entity.State;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StateRepository extends CrudRepository<State, Long>{

    public State findByName(String stateName);
    
}
