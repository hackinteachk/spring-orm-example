package io.muic.ooc.webapp.repository;

import io.muic.ooc.webapp.entity.Place;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlaceRepository extends CrudRepository<Place, Long> {

    public List<Place> findByName(String placeName);

    List<Place> findByNameContaining(String name);
}
