package io.muic.ooc.webapp.dto;

import io.muic.ooc.webapp.entity.User;

import java.util.Set;

/* DTO : Data Transfer Object */
public class UserWithProfileDto {

    private String name;
    private String displayName;
    private String telephone;
    private String address;
    private Set<String> roles;

    public UserWithProfileDto(User user){
        name = user.getName();
        displayName = user.getUsername();
        telephone = user.getProfile().getPhoneNumber();
        address = user.getProfile().getAddress();
        roles = user.getRoles();
    }

    public String getName() {
        return name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getAddress() {
        return address;
    }

    public Set<String> getRoles(){
        return roles;
    }

}
