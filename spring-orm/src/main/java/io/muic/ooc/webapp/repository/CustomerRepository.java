package io.muic.ooc.webapp.repository;

import io.muic.ooc.webapp.entity.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/*
* C - create
* R - read
* U - update
* D - delete
* */

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long> {

    List<Customer> findByLastName(String lastName);
}
