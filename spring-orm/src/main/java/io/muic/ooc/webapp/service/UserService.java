package io.muic.ooc.webapp.service;

import io.muic.ooc.webapp.entity.User;
import io.muic.ooc.webapp.repository.UserRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public User addRole(long id, String role){
        User user = userRepository.findOne(id);
        if(!StringUtils.isBlank(role) ||
                user.getRoles().contains(role)){

            user.getRoles().add(role);
            return userRepository.save(user);

        }
        return null;
    }
}
