package io.muic.ooc.webapp.controller;

import io.muic.ooc.webapp.dto.UserWithProfileDto;
import io.muic.ooc.webapp.entity.Customer;
import io.muic.ooc.webapp.entity.Profile;
import io.muic.ooc.webapp.entity.User;
import io.muic.ooc.webapp.entity.UserGroup;
import io.muic.ooc.webapp.repository.CustomerRepository;
import io.muic.ooc.webapp.repository.ProfileRepository;
import io.muic.ooc.webapp.repository.UserGroupRepository;
import io.muic.ooc.webapp.repository.UserRepository;
import io.muic.ooc.webapp.service.UserService;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class OrmController {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserGroupRepository userGroupRepository;

    @Autowired
    private ProfileRepository profileRepository;

    @Autowired
    private UserService userService;

    @GetMapping(value = {"/"})
    public String index(Model model) {
        Customer customer = new Customer();
        customer.setFirstName(RandomStringUtils.randomAlphabetic(5));
        customer.setLastName(RandomStringUtils.randomAlphabetic(5));

        customerRepository.save(customer);

        Iterable<Customer> customerIterable = customerRepository.findAll();

        List<Customer> customers = new ArrayList<>();
        for (Customer c : customerIterable) {
            customers.add(c);
        }

        model.addAttribute("customers", customers);

        return "hello";
    }

    @GetMapping(value = {"/addUser"})
    @ResponseBody
    public List<User> addUser(Model model) {

        UserGroup userGroup = new UserGroup();
        userGroup.setDescription("xxx");
        userGroup.setName(RandomStringUtils.randomAlphabetic(5));
        userGroupRepository.save(userGroup);

        User user = new User();
        user.setUsername(RandomStringUtils.randomAlphabetic(5));
        user.setName(RandomStringUtils.randomAlphabetic(5));
        user.setHashedPassword(RandomStringUtils.randomAlphabetic(20));
        user.getRoles().add("ADMIN");
        user.getGroups().add(userGroup);
        userRepository.save(user);

        Iterable<User> userIterable = userRepository.findAll();

        List<User> users = new ArrayList<>();
        for (User c : userIterable) {
            users.add(c);
        }

        return users;
    }

    @GetMapping("/exampleCreateUserAndProfile")
    public UserWithProfileDto ex(){
        /* local user */
        User user = new User();
        user.setName("Nuttapat");
        user.setUsername("hackinteach");
        /* reassign ID from database */
        /* 'user' proxy from database */
        user = userRepository.save(user);

        Profile profile = new Profile();
        profile.setAddress("Mars");
        profile.setPhoneNumber("000000");
        profile.setUser(user);
        profileRepository.save(profile);

        user.setProfile(profile);

        /* prevent from infinite loops (profile <-> user) */
        return new UserWithProfileDto(user);
    }

    /* http://localhost:8080/user?id=7 */
    @GetMapping(value="/user", params={"id","role"})
    @Transactional
    public UserWithProfileDto ex(
            @RequestParam long id,
            @RequestParam String role
    ){
//        User user = userRepository.findOne(id);
//
//        user.setName("xxxx");
//        user.getProfile().setPhoneNumber("9999");
//

//        Normally should save profile, not user
//        but this is automatically saved by Hibernate
//        userRepository.save(user);
        return new UserWithProfileDto(userService.addRole(id,role));
    }

    /* http://localhost:8080/user?id=7 */
    @GetMapping(value="/user" ,params={"id"})
    @Transactional
    public UserWithProfileDto ex(
            @RequestParam long id
    ){
        User user = userRepository.findOne(id);

        user.setName("xxxx");
        user.getProfile().setPhoneNumber("9999");
//

//        Normally should save profile, not user
//        Normally should save profile, not user
//        but this is automatically saved by Hibernate
        userRepository.save(user);
        return new UserWithProfileDto(user);
    }
}
