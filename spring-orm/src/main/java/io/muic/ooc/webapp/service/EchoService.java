package io.muic.ooc.webapp.service;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class EchoService {

    @Scheduled(fixedRate= 5000)
    public void doEcho(){
        System.out.println("Hello world!");
    }

}
