package io.muic.ooc.webapp.entity;

import javax.persistence.*;

@Entity
public class Credential {

    /* must have primary key */
    @Id
    @GeneratedValue
    private long id;

    private String method;

    private String credential;

    @ManyToOne
    @JoinTable(name = "join_user_credential",
            joinColumns = @JoinColumn(name = "credential_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private User owner;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getCredential() {
        return credential;
    }

    public void setCredential(String credential) {
        this.credential = credential;
    }
}
