package io.muic.ooc.webapp.entity;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author gigadot
 */
@Entity
public class UserGroup {

    @Id
    @GeneratedValue
    private Long id;

    private String description;

    private String name;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "join_user_group",
        inverseJoinColumns = @JoinColumn(name = "user_id"),
        joinColumns = @JoinColumn(name = "group_id")
    )
    private Set<User> users = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public Set<User> getUsers() {
//        return users;
//    }
//
//    public void setUsers(Set<User> users) {
//        this.users = users;
//    }
}
